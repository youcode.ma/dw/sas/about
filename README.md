# Self Assessment Skill  
Une période intensive pour s’outiller et s’engager dans sa formation  

## Découvrir et maîtriser son environnement de travail  

### Configuration des postes de travail : l’environnement Linux
### Ouverture à la culture et aux enjeux du numérique
### Présentation des langages de programmation
### Initiation à Git et GitHub : versionner son code, collaborer et contribuer à un projet open source
 

## Définir les objectifs de la formation

### Lancement du projet fil rouge semi-dirigé : la création d’une entreprise fictive
### Les principes de la gestion de projet : travailler en équipe tout au long de la formation / agile
### Réaliser son “arbre de compétence” qui suivra la promo tout au long de la formation
### Méteo Promo